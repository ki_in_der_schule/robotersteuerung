package robotersteuerung;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * Jedes Objekt dieser Klasse stellt eine Fuzzy-Mengen dar, welche aus einer 
 * oder mehreren Fuzzy-Variablen bestehen kann. 
 * 
 */
public class FuzzyMenge 
{
	/** Liste der Namen der zu dieser Fuzzy-Menge gehörigen Fuzzy-Variablen. */
	private List<String> variablenNamen;

	/** Liste der zu dieser Fuzzy-Menge gehörigen Fuzzy-Variablen. */
	private List<List<Punkt>> variablen;
	
	
	/**
	 * Erezeugt die Fuzzy-Menge aus einer Menge von Variablen mit zugehörigen Namen.
	 * 
	 * @param variablenNamen die Namen der Variablen
	 * @param variablen      die Variablen, aus denen die Fuzzy-Menge erzeugt wird
	 */
	public FuzzyMenge( List<String> variablenNamen, List<List<Punkt>> variablen )
	{
		this.variablenNamen = variablenNamen;
		this.variablen = variablen;
	}
	
	
	/**
	 * Gibt den maximalen Funktionswert für einen gegebenen x-Wert zurück.
	 * 
	 * @param x  gegebener x-Wert
	 * @return   zugehöriger maximaler Funktionswert
	 */
	public double f( double x )
	{
		// Wenn zu weit links/rechts, dann y-Wert des ersten/letzten Punktes zurückgeben
		if( x < variablen.get( 0 ).get( 0 ).getX() )
		{
			return( variablen.get( 0 ).get( 0 ).getY() );
		}
		else if( x > variablen.get( variablen.size() - 1 ).get( variablen.get( variablen.size() - 1 ).size() - 1 ).getX() )
		{
			return( variablen.get( variablen.size() - 1 ).get( variablen.get( variablen.size() - 1 ).size() - 1 ).getY() );
		}
			
		// Alle Variablen sammeln, in denen der x-Wert liegt
		List<List<Punkt>> trefferVariablen = new ArrayList<List<Punkt>>();
		for( List<Punkt> variable : variablen )
		{			
			if( x >= variable.get( 0 ).getX() && x < variable.get( variable.size() - 1 ).getX() )
			{
				trefferVariablen.add( variable );
			}
		}
		
		// Größten y-Wert bestimmen
		double maxY = Double.NEGATIVE_INFINITY;
		for( List<Punkt> variable : trefferVariablen )
		{			
			for( int i = 0; i < variable.size() - 1; i++ )
			{
				Punkt punkt = variable.get( i );
				Punkt punkt2 = variable.get( i + 1 );
				if( x >= punkt.getX() && x < punkt2.getX() )
				{
					// Linear interpolieren
					double x1 = punkt.getX();
					double y1 = punkt.getY();
					double x2 = variable.get( variable.indexOf( punkt ) + 1 ).getX();
					double y2 = variable.get( variable.indexOf( punkt ) + 1 ).getY();
					double steigung = (y2 - y1) / (x2 - x1);
					double y = steigung * (x - punkt.getX()) + y1;
					maxY = Math.max( y, maxY );
				}
			}
		}
		
		return( maxY );
	}
	
	
	/**
	 * Gibt die bei gegebenem x aktivierten Variablen mit zugehörigen Namen zurück.
	 * 
	 * @param x  gegebener x-Wert
	 * @return   HashMap mit Variablennamen als Schlüssel und zugehörigen Variablen als Werte 
	 */
	public HashMap<String, Double> aktivierung( double x )
	{
		LinkedHashMap<String, Double> aktivierteVariablen = new LinkedHashMap<String, Double>();

		// Wenn zu weit links/rechts, dann y-Wert des ersten/letzten Punktes zurückgeben
		if( x < variablen.get( 0 ).get( 0 ).getX() )
		{
			aktivierteVariablen.put( variablenNamen.get( 0 ), variablen.get( 0 ).get( 0 ).getY() );
			return( aktivierteVariablen );
		}
		else if( x > variablen.get( variablen.size() - 1 ).get( variablen.get( variablen.size() - 1 ).size() - 1 ).getX() )
		{
			aktivierteVariablen.put( variablenNamen.get( variablen.size() - 1 ), variablen.get( variablen.size() - 1 ).get( variablen.get( variablen.size() - 1 ).size() - 1 ).getY() );
			return( aktivierteVariablen );
		}
		
		// Alle Variablen, in denen der x-Wert liegt, mit zugehörigem y-Wert sammeln
		for( List<Punkt> variable : variablen )
		{			
			if( x >= variable.get( 0 ).getX() && x < variable.get( variable.size() - 1 ).getX() )
			{
				for( int i = 0; i < variable.size() - 1; i++ )
				{
					Punkt punkt = variable.get( i );
					Punkt punkt2 = variable.get( i + 1 );
					if( x >= punkt.getX() && x < punkt2.getX() )
					{
						// Aktivierungsgrad berechnen
						double x1 = punkt.getX();
						double y1 = punkt.getY();
						double x2 = variable.get( variable.indexOf( punkt ) + 1 ).getX();
						double y2 = variable.get( variable.indexOf( punkt ) + 1 ).getY();
						double steigung = (y2 - y1) / (x2 - x1);
						double y = steigung * (x - punkt.getX()) + y1;
						aktivierteVariablen.put( variablenNamen.get( variablen.indexOf( variable ) ) , y );
					}
				}
			}
		}
		
		return( aktivierteVariablen );
	}
	
	
	/**
	 * Führt einen Alpha-Schnitt auf Basis der aktivierten Variablen mit zugehörigen Aktivierungsgraden durch.
	 * 
	 * @param aktivierteVariablen  HashMap mit den Namen der aktivierten Varialben als Schlüssel und zugehörigen Aktivierungsgrade als Werte
	 * @return                     den Alpha-Schnitt der Fuzzy-Menge
	 */
	public FuzzyMenge alphaSchnitt( HashMap<String, Double> aktivierteVariablen )
	{
		// Listen mit der maximal möglichen Anzahl verbleibender Variablen erzeugen
		List<List<Punkt>> verbleibendeVariablen = new ArrayList<List<Punkt>>();
		List<String> verbleibendeVariablenNamen = new ArrayList<String>();
		for( int i = 0; i < variablenNamen.size(); i++ )
		{
			verbleibendeVariablen.add( null );
			verbleibendeVariablenNamen.add( null );
		}
		
		// Neue benötigte Punkte in den verbliebenen Variablen berechnen
		for( String variablenName : aktivierteVariablen.keySet() )
		{
			List<Punkt> alteVariable = variablen.get( variablenNamen.indexOf( variablenName ) );
			List<Punkt> neueVariable = new ArrayList<Punkt>();
			for( int i = 0; i < alteVariable.size(); i++ )
			{
				neueVariable.add( alteVariable.get( i ) );
			}
		
			for( int i = 0; i < alteVariable.size() - 1; i++ )
			{
				if(     (alteVariable.get( i ).getY() < aktivierteVariablen.get( variablenName )
					     && alteVariable.get( i + 1 ).getY() > aktivierteVariablen.get( variablenName ) ) 
					||  (alteVariable.get( i ).getY() > aktivierteVariablen.get( variablenName )
				         && alteVariable.get( i + 1 ).getY() < aktivierteVariablen.get( variablenName ) ) )
				{
					// Schnittpunkt berechnen
					double x1 = alteVariable.get( i ).getX();
					double y1 = alteVariable.get( i ).getY();
					double x2 = alteVariable.get( i + 1 ).getX();
					double y2 = alteVariable.get( i + 1 ).getY();
					double steigung = (y2 - y1) / (x2 - x1);
					double y = aktivierteVariablen.get( variablenName );
					double x = ((y - y1) / steigung) + alteVariable.get( i ).getX();
				
					neueVariable.add( neueVariable.indexOf( alteVariable.get( i + 1 ) ), new Punkt( x, y ) );
				}
			}
			
			// Sollte der erste oder der letzte Punkt oberhalb des Alpha-Schnitts liegen, durch
			// entsprechende Punkte auf Alpha-Niveau ersetzen
			if( neueVariable.get( 0 ).getY() > aktivierteVariablen.get( variablenName ) )
			{
				double x = neueVariable.get( 0 ).getX();
				neueVariable.remove( 0 );
				neueVariable.add( 0, new Punkt( x, aktivierteVariablen.get( variablenName ) ) );
			}
			if( neueVariable.get( neueVariable.size() - 1 ).getY() > aktivierteVariablen.get( variablenName ) )
			{
				double x = neueVariable.get( neueVariable.size() - 1 ).getX();
				neueVariable.remove( neueVariable.size() - 1 );
				neueVariable.add( new Punkt( x, aktivierteVariablen.get( variablenName ) ) );
			}
			
			// Alle verbleibenden Punkte oberhalb des Alpha-Schnitts entfernen
			for( int i = 0; i < neueVariable.size(); i++ )
			{
				if( neueVariable.get( i ).getY() > aktivierteVariablen.get( variablenName ) )
				{
					neueVariable.remove( i );
					i--;
				}
			}
			
			// Verbleibende Variablen an entsprechender Stelle einordnen
			verbleibendeVariablenNamen.add( variablenNamen.indexOf( variablenName ), variablenName );
			verbleibendeVariablen.add( variablenNamen.indexOf( variablenName ), neueVariable );
		}

		// Alle unbesetzten Variablenplätze entfernen
		for( int i = 0; i < verbleibendeVariablenNamen.size(); i++ )
		{
			if( verbleibendeVariablenNamen.get( i ) == null )
			{
				verbleibendeVariablenNamen.remove( i );
				verbleibendeVariablen.remove( i );
				i--;
			}
		}
		
		return( new FuzzyMenge( verbleibendeVariablenNamen, verbleibendeVariablen ) );
	}
	
	
	/**
	 * Führt eine Schwerpunktberechnung der Fuzzy-Menge durch.
	 * 
	 * @return  den Schwerpunkt der Fläche der Fuzzy-Menge
	 */
	public Punkt schwerpunkt()
	{
		double schritt = 0.01;
		
		// Grenzen
		double a = variablen.get( 0 ).get( 0 ).getX();
		double b = variablen.get( variablen.size() - 1 ).get( variablen.get( variablen.size() - 1 ).size() - 1 ).getX();

		
		//
		// x-Koordinate berechnen
		//
		
		// Fläche von x * f( x ) berechnen
		double flächeXMalFvonX = 0;
		for( double x = a; x < b; x += schritt )
		{
			double x1 = x;
			double x2 = x + schritt;
			flächeXMalFvonX += x * f( x ) * (x2 - x1);
		}
		
		// Fläche von f( x ) berechnen
		double flächeFvonX = 0;
		for( double x = a; x < b; x += schritt )
		{
			double x1 = x;
			double x2 = x + schritt;
			flächeFvonX += f( x ) * (x2 - x1);
		}

		double schwerpunktX = flächeXMalFvonX / flächeFvonX;
		
		
		//
		// y-Koordinate berechnen
		//
		
		// Fläche von 1/2 * f( x ) ^ 2  berechnen
		double flächeEinHalbFvonXQuadrat = 0;
		for( double x = a; x < b; x += schritt )
		{
			double x1 = x;
			double x2 = x + schritt;
			flächeEinHalbFvonXQuadrat += f( x ) * f( x ) * (x2 - x1);
		}
		flächeEinHalbFvonXQuadrat *= 0.5;
		
		double schwerpunktY = flächeEinHalbFvonXQuadrat / flächeFvonX;
		
		
		return( new Punkt( schwerpunktX, schwerpunktY ) );
	}
}
