package robotersteuerung;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Diese Klasse dient der Steuerung des Roboters.
 * Die Steuerung des Roboters muss in der Methode mit dem Namen "berechnen" festgelegt werden.
 * 
 */
public class RoboterSteuerung 
{
	/** Fuzzy-Eingangsmenge */
	private FuzzyMenge eingangsMenge;
	
	/** Fuzzy-Ausgangsmenge */
	private FuzzyMenge ausgangsMenge;

	
	/**
	 * Konstruktor der Robotersteuerung zum Erzeugen der benötigten Bestandteile.
	 */
	public RoboterSteuerung()
	{
		eingangsMengenErzeugen();
		ausgangsMengenErzeugen();
	}
	
	
	/**
	 * Erzeugt die Eingangsmenge(n).
	 */
	public void eingangsMengenErzeugen()
	{
		//
		// Fuzzy-Variablen modellieren
		//
		
		List<List<Punkt>> intervalleEingang = new ArrayList<List<Punkt>>();
		
		List<Punkt> weitLinks = new ArrayList<Punkt>();
		weitLinks.add( new Punkt( -20, 1 ) );
		weitLinks.add( new Punkt( -10, 0 ) );
		intervalleEingang.add( weitLinks );
		
		List<Punkt> links = new ArrayList<Punkt>();
		links.add( new Punkt( -20, 0 ) );
		links.add( new Punkt( -10, 1 ) );
		links.add( new Punkt( 0, 0 ) );
		intervalleEingang.add( links );
		
		List<Punkt> mittig = new ArrayList<Punkt>();
		mittig.add( new Punkt( -10, 0 ) );
		mittig.add( new Punkt( 0, 1 ) );
		mittig.add( new Punkt( 10, 0 ) );
		intervalleEingang.add( mittig );
		
		List<Punkt> rechts = new ArrayList<Punkt>();
		rechts.add( new Punkt( 0, 0 ) );
		rechts.add( new Punkt( 10, 1 ) );
		rechts.add( new Punkt( 20, 0 ) );
		intervalleEingang.add( rechts );
		
		List<Punkt> weitRechts = new ArrayList<Punkt>();
		weitRechts.add( new Punkt( 10, 0 ) );
		weitRechts.add( new Punkt( 20, 1 ) );
		intervalleEingang.add( weitRechts );
		
		
		// Namen für Fuzzy-Variablen vergeben
		List<String> intervallNamenEingang = new ArrayList<String>();
		intervallNamenEingang.add( "weit_links" );
		intervallNamenEingang.add( "links" );
		intervallNamenEingang.add( "mittig" );
		intervallNamenEingang.add( "rechts" );
		intervallNamenEingang.add( "weit_rechts" );
		
		// Eingangsmenge erzeugen
		eingangsMenge = new FuzzyMenge( intervallNamenEingang, intervalleEingang );
	}
	

	/**
	 * Erzeugt die Ausgangsmenge(n).
	 */
	public void ausgangsMengenErzeugen()
	{
		//
		// Fuzzy-Variablen modellieren
		//
		
		List<List<Punkt>> intervalleAusgang = new ArrayList<List<Punkt>>();
		
		List<Punkt> lenkStarkRechts = new ArrayList<Punkt>();
		lenkStarkRechts.add( new Punkt( -20, 1 ) );
		lenkStarkRechts.add( new Punkt( -10, 0 ) );
		intervalleAusgang.add( lenkStarkRechts );
		
		List<Punkt> lenkRechts = new ArrayList<Punkt>();
		lenkRechts.add( new Punkt( -20, 0 ) );
		lenkRechts.add( new Punkt( -10, 1 ) );
		lenkRechts.add( new Punkt( 0, 0 ) );
		intervalleAusgang.add( lenkRechts );
		
		List<Punkt> lenkGerade = new ArrayList<Punkt>();
		lenkGerade.add( new Punkt( -10, 0 ) );
		lenkGerade.add( new Punkt( 0, 1 ) );
		lenkGerade.add( new Punkt( 10, 0 ) );
		intervalleAusgang.add( lenkGerade );
		
		List<Punkt> lenkLinks = new ArrayList<Punkt>();
		lenkLinks.add( new Punkt( 0, 0 ) );
		lenkLinks.add( new Punkt( 10, 1 ) );
		lenkLinks.add( new Punkt( 20, 0 ) );
		intervalleAusgang.add( lenkLinks );
		
		List<Punkt> lenkStarkLinks = new ArrayList<Punkt>();
		lenkStarkLinks.add( new Punkt( 10, 0 ) );
		lenkStarkLinks.add( new Punkt( 20, 1 ) );
		intervalleAusgang.add( lenkStarkLinks );
		
		
		// Namen für Fuzzy-Variablen vergeben
		List<String> intervallNamenAusgang = new ArrayList<String>();
		intervallNamenAusgang.add( "stark_links" );
		intervallNamenAusgang.add( "links" );
		intervallNamenAusgang.add( "gerade" );
		intervallNamenAusgang.add( "rechts" );
		intervallNamenAusgang.add( "stark_rechts" );
		
		// Ausgangsmenge erzeugen
		ausgangsMenge = new FuzzyMenge( intervallNamenAusgang, intervalleAusgang );
	}
	
	
	/**
	 * Methode zur Steuerung des Roboters.
	 * Es wird die vom Roboter wahrgenommene Position auf der Straße übergeben. 
	 * Der Roboter kann dann über den Rückgabewert seine Bewegung in x-Richtung festlegen. Gültige Werte gehen dabei von -20 (für stark nach links lenken) bis +20 (für stark nach rechts lenken).
	 * 
	 * @param positionX   enthält die x-Position des Roboters auf der Straße (von -20 bis +20)
	 * @return            die Bewegung des Roboters in x-Richtung (von -20 bis +20)
	 */
	public int berechnen( double positionX )
	{
		// Hier kann die Steuerung des Roboters programmiert werden!
		// ...
		
		// Beispiel (für Fuzzy-Steuerung zu "berechnenFuzzy" ändern)
		return( berechnenHart( positionX ) );
	}
	
	
	/**
	 * Fuzzy-Steuerung des Roboters  (kann in Funktion berechnen() eingesetzt werden).
	 * 
	 * @param positionX   enthält die x-Position des Roboters auf der Straße (von -20 bis +20)
	 * @return            die Bewegung des Roboters in x-Richtung (von -20 bis +20)
	 */
	private int berechnenFuzzy( double positionX )
	{
		// Aktivierte Fuzzy-Variablen in der Eingangsmenge bestimmen
		HashMap<String, Double> aktiviert = eingangsMenge.aktivierung( positionX );
		
		// Logische Zuordnung vornehmen:
		HashMap<String, Double> ausgangAktiviert = new HashMap<String, Double>();
		for( String eingangsName : aktiviert.keySet() )
		{
			String ausgangsName = null;

			// weit links => stark rechts lenken
			if( eingangsName.contains( "weit_links" ) )
			{
				ausgangsName = "stark_rechts";
			}

			// links => rechts lenken
			else if( eingangsName.contains( "links" ) )
			{
				ausgangsName = "rechts";
			}

			// mittig => gerade aus fahren
			else if( eingangsName.contains( "mittig" ) )
			{
				ausgangsName = "gerade";
			}

			// weit rechts => stark links lenken
			else if( eingangsName.contains( "weit_rechts" ) )
			{
				ausgangsName = "stark_links";
			}

			// rechts => links lenken
			else if( eingangsName.contains( "rechts" ) )
			{
				ausgangsName = "links";
			}
			
			ausgangAktiviert.put( ausgangsName, aktiviert.get( eingangsName ) );
		}
		
		// Alpha-Schnitt
		FuzzyMenge alpha = ausgangsMenge.alphaSchnitt( ausgangAktiviert );
		
		// Defuzzyfizieren (durch Schwerpunktberechnung)
		Punkt punkt = alpha.schwerpunkt();
		
		return( (int) Math.round( punkt.getX() ) );	
	}

	
	/**
	 * Steuerung des Roboters mit "harter" Logik (kann in Funktion berechnen() eingesetzt werden).
	 * 
	 * @param positionX   enthält die x-Position des Roboters auf der Straße (von -20 bis +20)
	 * @return            die Bewegung des Roboters in x-Richtung (von -20 bis +20)
	 */
	private int berechnenHart( double positionX )
	{
		// Logische Zuordnung vornehmen:

		// weit links => stark rechts lenken
		if( positionX < -10 )
		{
			return( 20 );
		}

		// links => rechts lenken
		else if( positionX < 0 )
		{
			return( 10 );
		}

		// mittig => gerade aus fahren
		else if( positionX == 0 )
		{
			return( 0 );
		}

		// rechts => links lenken
		else if( positionX < 10 )
		{
			return( -10 );
		}

		// weit rechts => stark links lenken
		else
		{
			return( -20 );
		}
	}
}
