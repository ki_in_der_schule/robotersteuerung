package robotersteuerung;


/**
 * Einfache Klasse für 2D Punkte.
 * 
 */
public class Punkt 
{
	/** Die x-Koordinate. */
	private double x;

	/** Die y-Koordinate. */
	private double y;
	
	
	/**
	 * Konstruktor zum erzeugen eines Punktes.
	 * 
	 * @param x  x-Koordinate
	 * @param y  y-Koordinate
	 */
	public Punkt( double x, double y )
	{
		this.x = x;
		this.y = y;
	}

	
	/**
	 * Kopierkonstruktur zum kopieren eines Punktes.
	 * 
	 * @param punkt  der zu kopierende Punkt
	 */
	public Punkt( Punkt punkt )
	{
		this.x = punkt.x;
		this.y = punkt.y;
	}
	
	
	/**
	 * Gibt die x-Koordinate zurück.
	 * 
	 * @return  die x-Koordinate
	 */
	public double getX() 
	{
		return x;
	}

	
	/**
	 * Gibt die y-Koordinate zurück.
	 * 
	 * @return  die y-Koordinate
	 */
	public double getY() 
	{
		return y;
	}
}
