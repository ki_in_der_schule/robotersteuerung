package robotersteuerung;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


/**
 * Ein Objekt dieser Klasse stellt ein Spielfeld dar. VERSION: 1.0.4
 * <p>
 * Ein Spielfeld besteht aus einem Fenster, welchem Spielobjekte und Bedienelemente 
 * (wie zum Beispiel Buttons und Textelemente) hinzugefügt werden können. 
 * <p>
 * Im Hauptprogramm sollte zunächst ein Spielfeld-Objekt erzeugt werden. In der 
 * Hauptschleife des Spiels sollte dann bei jedem Schleifendurchlauf das Spielfeld
 * mit der Methode "zeichnen()" gezeichnet werden. Mit der Method "warten()" ist es
 * möglich die Ablaufgeschwindigkeit des Spiels festzulegen. 
 * <p>
 * Ein einfaches Hauptprogramm, welches das Spielfeld verwendet könnte wie folgt 
 * aussehen:
 * <pre>
 * <code>
 * public static void main( String[] args )
 * {
 *     // Spielfeld erzeugen
 *     Spielfeld spielfeld = new Spielfeld();
 *
 *     // Hauptschleife des Spiels
 *     while( true )
 *     {
 *         // Hier wird der Spielablauf programmiert
 *         // ...
 *     
 *         // Das Spielfeld neu zeichnen
 *         spielfeld.zeichnen();
 *         
 *         // Etwas warten
 *         spielfeld.warten( 50 );
 *     }
 * }
 * </code>
 * </pre>
 * 
 * @see Spielfeld#zeichnen()
 * @see Spielfeld#warten( int )
 * @see Spielobjekt
 * 
 * @author Daan Apeldoorn
 *
 */
public class Spielfeld extends Frame implements WindowListener, KeyListener, MouseListener, MouseMotionListener
{	
	/** Höhe des Spielfeld-Fensters. */ 
	public static final int höhe = 600;

	/** Breite des Spielfeld-Fensters. */ 
	public static final int breite = 800;

	/** Die momentan gedrückte Taste. */
	public String tasteGedrückt = "";

	/** Gibt an, ob die Pfeiltaste nach links gedrückt wird. */
	public boolean linksGedrückt = false;

	/** Gibt an, ob die Pfeiltaste nach rechts gedrückt wird. */
	public boolean rechtsGedrückt = false;

	/** Gibt an, ob die Pfeiltaste nach oben gedrückt wird. */
	public boolean obenGedrückt = false;

	/** Gibt an, ob die Pfeiltaste nach unten gedrückt wird. */
	public boolean untenGedrückt = false;
	
	/** Gibt an, ob die linke Maustaste gedrückt wird. */
	public boolean linksGeklickt = false;

	/** Gibt an, ob die rechte Maustaste gedrückt wird. */
	public boolean rechtsGeklickt = false;

	/** Die momentane x-Koordinate des Mauszeigers auf dem Spielfeld. */
	public int mausX = 0;

	/** Die momentane y-Koordinate des Mauszeigers auf dem Spielfeld. */
	public int mausY = 0;
	
	/** Das Spielbojekt auf dem Spielfeld, auf welches der Mauszeiger momentan zeigt. "null", wenn der Mauszeiger auf kein Spielobjekt zeigt. */
	public Spielobjekt spielobjektBeiMaus = null;
	
	/** Liste der Spielobjekte auf dem Spielfeld. */
	private List<Spielobjekt> spielobjekte;

	/** Liste der Bilder der Spielobjekte auf dem Spielfeld. */
	private List<Map<String, Image>> bilder;

	/** Liste der Klänge, die geladen wurden. */
	private static List<AudioClip> klänge = new ArrayList<AudioClip>();
	
	/** Liste der Namen der Klänge, die geladen wurden. */
	private static List<String> klangnamen = new ArrayList<String>();
	
	/** Der Grafikkontext des Backbuffers für das Double-Buffering des Spielfelds. */
	private Graphics gBackBuffer;
	
	/** Der Backbuffers für das Double-Buffering des Spielfelds. */
	private Image backBuffer;
	
	/** 
	 * Der Zeitpunkt des letzten Aufrufs der Methode "warten( int millisekunden )".
	 *  
	 * @see Spielfeld#warten( int )
	 */
	long vorherigerZeitpunkt = 0;
	
	/** Der Zufallsgenerator, welcher zur Programmierung eines Spiels verwendet werden kann. */
	private static Random zufallsgenerator = new Random();
	
	
	/**
	 * Konstruktor des Spielfeldes: Erzeugt ein neues Spielfeld. 
	 */
	public Spielfeld()
	{
		// Fenster öffnen und einstellen
		super( "Spiel" );
        setBackground( Color.white );
        setLayout( null );
    	Toolkit toolkit = Toolkit.getDefaultToolkit();
    	setLocation( toolkit.getScreenSize().width / 2 - breite / 2, toolkit.getScreenSize().height / 2 - höhe / 2 );
        setResizable( false );
        addWindowListener( this );
        addKeyListener( this );
        addMouseListener( this );
        addMouseMotionListener( this );
        setSize( breite, höhe );
        setVisible( true ); 
        setIgnoreRepaint( true );

        // Liste für Spielobjekte erzeugen
        spielobjekte = new ArrayList<Spielobjekt>();

        // Liste für Bilder erzeugen
        bilder = new ArrayList<Map<String, Image>>();
        
        // Grafikverarbeitung erstellen
        backBuffer = createImage( breite, höhe );
        gBackBuffer = backBuffer.getGraphics();
	}
	
	
	/**
	 * Fügt dem Spielfeld ein neues Spielobjekt hinzu.
	 * <p>
	 * Es werden ebenfalls alle Unterobjekte des Spielobjektes rekursiv hinzugefügt. 
	 * 
	 * @param spielobjekt das hinzuzufügende Spielobjekt
	 */
	public synchronized void hinzufügen( Spielobjekt spielobjekt )
	{
		spielobjekte.add( spielobjekt );

		// Wenn Spielobjekt ein Bild hat
		if( (spielobjekt.bildname != null) && (!spielobjekt.bildname.equals( "" )) )
		{
			// Bild für Spielobjekt laden
			Image bild = bildLaden( spielobjekt.bildname );
			
		    // Bild hinzufügen
		    Map<String, Image> frames = new HashMap<String, Image>();
		    frames.put( spielobjekt.bildname, bild );
		    bilder.add( frames );
		}
		else
		{
		    Map<String, Image> frames = new HashMap<String, Image>();
		    frames.put( spielobjekt.bildname, null );
			bilder.add( frames );
		}
		
	    // Rekursiv alle untergeordneten Spielobjekte hinzufügen
	    for( int i = 0; i < spielobjekt.getClass().getFields().length; i++ )
	    {
	    	try
	    	{
		    	if( spielobjekt.getClass().getFields()[ i ].get( spielobjekt ) instanceof Spielobjekt )
		    		hinzufügen( (Spielobjekt) spielobjekt.getClass().getFields()[ i ].get( spielobjekt ) );
	    	}
	    	catch( IllegalAccessException e )
	    	{
	    		System.out.println( "IllegalAccessException beim Hinzufügen eines untergeordneten Spielobjekts." );
	    	}
	    	catch( IllegalArgumentException e )
	    	{
	    		System.out.println( "IllegalArgumentException beim Hinzufügen eines untergeordneten Spielobjekts." );
	    	}
	    }
	}
	

	/**
	 * Entfernt ein Spielobjekt vom Spielfeld.
	 * <p>
	 * Es werden ebenfalls alle Unterobjekte des Spielobjektes rekursiv entfernt.
	 * 
	 * @param spielobjekt das zu entfernende Spielobjekt
	 */
	public synchronized void entfernen( Spielobjekt spielobjekt )
	{
		// Position des Spielobjekts in Liste merken
		int löschindex = spielobjekte.indexOf( spielobjekt );
		
		// Spielobjekt aus Liste entfernen
		spielobjekte.remove( spielobjekt );

	    // Zugehöriges Bild löschen
		bilder.remove( löschindex );
	    
	    // Rekursiv alle untergeordneten Spielobjekte löschen
	    for( int i = 0; i < spielobjekt.getClass().getFields().length; i++ )
	    {
	    	try
	    	{
		    	if( spielobjekt.getClass().getFields()[ i ].get( spielobjekt ) instanceof Spielobjekt )
		    		entfernen( (Spielobjekt) spielobjekt.getClass().getFields()[ i ].get( spielobjekt ) );
	    	}
	    	catch( IllegalAccessException e )
	    	{
	    		System.out.println( "IllegalAccessException beim Entfernen eines untergeordneten Spielobjekts." );
	    	}
	    	catch( IllegalArgumentException e )
	    	{
	    		System.out.println( "IllegalArgumentException beim Entfernen eines untergeordneten Spielobjekts." );
	    	}
	    }
	}

	
	/**
	 * Zeichnet das Spielfeld mit allen seinen momentan enthaltenen Spielobjekten.
	 * <p>
	 * Diese Methode sollte einmal in jedem Durchlauf der Spielschleife aufgerufen werden.
	 * 
	 * @see Spielfeld
	 */
	public synchronized void zeichnen()
	{
    	// Fenster neu zeichnen
    	repaint();

    	// Spielobjekt auf das der Mauszeiger zeigt merken
		spielobjektBeiMaus = null;
		for( int i = (spielobjekte.size() - 1); i >= 0 ; i-- )
		{
			if(    (spielobjekte.get( i ).unsichtbar == false)
				&& (mausX >= spielobjekte.get( i ).positionX) && (mausX <= (spielobjekte.get( i ).positionX + spielobjekte.get( i ).breite))
				&& (mausY >= spielobjekte.get( i ).positionY) && (mausY <= (spielobjekte.get( i ).positionY + spielobjekte.get( i ).höhe)) )
			{
				spielobjektBeiMaus = spielobjekte.get( i );
				break;
			}
		}
	}

	
	/**
	 * Wartet solange, bis die in Millisekunden angegebene Zeit seit dem letzten Aufruf der Methode verstichen ist.
	 * <p>
	 * Diese Methode dient dem Timing des Spiels. Jeder Schleifendurchlauf sollte gleich lang dauern und etwas 
	 * gebremst werden, damit das Spiel in gleichmäßig und nicht zu schnell abäuft.
	 * <p>
	 * Diese Methode sollte einmal in jedem Durchlauf der Spielschleife aufgerufen werden.
	 * 
	 * @param millisekunden
	 * 
	 * @see Spielfeld
	 */
	public void warten( int millisekunden )
	{
		try
		{
			long wartezeit = millisekunden - (System.currentTimeMillis() - vorherigerZeitpunkt);
			if( wartezeit > 0 )
				Thread.sleep( wartezeit );
		}
		catch( InterruptedException e )
		{
			System.out.println( "Unterbrechung beim Warten." );
		}
		
    	vorherigerZeitpunkt = System.currentTimeMillis();
	}
	
	
	/**
	 * Gibt eine Zufallszahl im angegebenen Bereich zurück (jeweils einschließlich der angegebenen Grenzen).
	 * 
	 * @param von Untergrenze des Bereichs
	 * @param bis Obergrenze des Bereichs
	 * @return eine Zufallszahl im angegebenen Bereich (jeweils einschließlich der angegebenen Grenzen)
	 */
	public static int zufallszahl( int von, int bis )
	{
		// Eine zufällige Zahl erzeugen
		int zahl = Math.abs( zufallsgenerator.nextInt() ) % (bis - von + 1) + von;
		
		// Die erzeugte Zahl zurückgeben
		return( zahl );
	}
	
	
	public synchronized void paint( Graphics g )
	{
        if( gBackBuffer == null )
        	return;
		
		// Bildschirm löschen
		gBackBuffer.clearRect( 0, 0, breite, höhe );

		// Bilder nachladen, wenn sie sich geändert haben und noch nicht existieren
    	for( int i = 0; i < spielobjekte.size(); i++ )
    	{
    		if( !bilder.get( i ).containsKey( spielobjekte.get( i ).bildname ) )
    		{
    			if( (spielobjekte.get( i ).bildname != null) && (!spielobjekte.get( i ).bildname.equals( "" )) )
    			{
    				// Neues Bild für Spielobjekt laden
    				Image bild = bildLaden( spielobjekte.get( i ).bildname );
    			    
    				// Den Bildern des Spielobjekts hinzufügen
    				bilder.get( i ).put( spielobjekte.get( i ).bildname, bild );
    			}
    			else
    			{
    			    bilder.get( i ).put( spielobjekte.get( i ).bildname, null );
    			}
    		}
    	}
		
		// Bilder zeichnen
    	for( int i = 0; i < bilder.size(); i++ )
    	{
    		// Wenn Spielobjekt nicht unsichtbar, dann zeichnen
	    	if( !spielobjekte.get( i ).unsichtbar )
	    	{
	    		// Wenn Spielobjekt ein Bild hat
	    		if( bilder.get( i ).get( spielobjekte.get( i ).bildname ) != null )
	    			gBackBuffer.drawImage( bilder.get( i ).get( spielobjekte.get( i ).bildname ), spielobjekte.get( i ).positionX, spielobjekte.get( i ).positionY, spielobjekte.get( i ).breite, spielobjekte.get( i ).höhe, this );
	
		        // Wenn vorhanden, Text anzeigen
				if( (spielobjekte.get( i ).text != null) && (!spielobjekte.get( i ).text.equals( "" )) )
				{
					// Wenn vorhanden, Farbe setzen...
					if( spielobjekte.get( i ).textfarbe != null )
						gBackBuffer.setColor( spielobjekte.get( i ).textfarbe );
					
					// ...sonst Farbe auf schwarz setzen
					else
						gBackBuffer.setColor( Color.BLACK );
					
					gBackBuffer.drawString( "" + spielobjekte.get( i ).text, spielobjekte.get( i ).positionX, spielobjekte.get( i ).positionY );
				}
	    	}
    	}
        
        // Bildschirm anzeigen
        g.drawImage( backBuffer, 0, 0, this );
	}

	
	public void update( Graphics g )
    {
         paint( g );
    } 

	
	public void windowClosing(WindowEvent event) 
	{ 
		// Beim schließen Programm beenden
		System.exit(0); 
	}
    
	
	public void windowClosed(WindowEvent event)
	{
	}
	
	
    public void windowDeiconified(WindowEvent event)
    {
    }
    
    
    public void windowIconified(WindowEvent event)
    {
    }
    
    
    public void windowActivated(WindowEvent event)
    {
    }
    
    
    public void windowDeactivated(WindowEvent event)
    {
    }
    
    
    public void windowOpened(WindowEvent event)
    {
    }


	public void keyPressed(KeyEvent event) 
	{
		// Gedrückte Pfeiltaste verarbeiten
		if( event.getKeyCode() == KeyEvent.VK_LEFT )
			linksGedrückt = true;
		else if( event.getKeyCode() == KeyEvent.VK_RIGHT )
			rechtsGedrückt = true;
		else if( event.getKeyCode() == KeyEvent.VK_UP )
			obenGedrückt = true;
		else if( event.getKeyCode() == KeyEvent.VK_DOWN )
			untenGedrückt = true;
		else
			// Gedrückte Taste verarbeiten
			tasteGedrückt =  "" + event.getKeyChar();
	}

	public void keyReleased(KeyEvent event) 
	{
		// Losgelassene Pfeiltaste verarbeiten
		if( event.getKeyCode() == KeyEvent.VK_LEFT )
			linksGedrückt = false;
		else if( event.getKeyCode() == KeyEvent.VK_RIGHT )
			rechtsGedrückt = false;
		else if( event.getKeyCode() == KeyEvent.VK_UP )
			obenGedrückt = false;
		else if( event.getKeyCode() == KeyEvent.VK_DOWN )
			untenGedrückt = false;
		else
			// Losgelassene Taste verarbeiten
			tasteGedrückt =  "";
	}

	public void keyTyped(KeyEvent event) 
	{
	}


	public void mouseClicked(MouseEvent event) 
	{
	}

	public void mouseEntered(MouseEvent event) 
	{
	}

	public void mouseExited(MouseEvent event) 
	{
	}

	public void mousePressed(MouseEvent event) 
	{
		// Mausklicks verarbeiten
		if( event.getButton() == MouseEvent.BUTTON1 )
			linksGeklickt = true;
		else if( event.getButton() == MouseEvent.BUTTON3 )
			rechtsGeklickt = true;
	}

	public void mouseReleased(MouseEvent event) 
	{
		// Mausklicks verarbeiten
		if( event.getButton() == MouseEvent.BUTTON1 )
			linksGeklickt = false;
		else if( event.getButton() == MouseEvent.BUTTON3 )
			rechtsGeklickt = false;
	}


	public void mouseDragged(MouseEvent event) 
	{
	}

	public void mouseMoved(MouseEvent event) 
	{
		mausX = event.getX();
		mausY = event.getY();
	}
	
	
	/**
	 * Spielt den Klang mit dem angegebenen Dateinamen ab. 
	 * <p>
	 * Es kann festgelegt werden, ob der Klang einmalig oder fortlaufend in einer 
	 * Schleife abgespielt werden soll.
	 * 
	 * @param dateiname der Dateiname der abzuspielenden Klangdatei
	 * @param wiederholen legt fest, ob der Klang einmalig oder fortlaufend in einer Schleife abgespielt werden soll
	 */
	public static synchronized void klangAbspielen( String dateiname, boolean wiederholen )
	{
		// Wenn es schon einen slchen Klang gibt, diesen verwenden
		AudioClip klang = null;
		for( int i = 0; i < klangnamen.size(); i++ )
		{
			if( klangnamen.get( i ).equals( dateiname ) )
			{
				klang = klänge.get( i );
				klang.stop();
				break;
			}
		}
		
		// Wenn es noch keinen solchen Klang gab, einen neuen erstellen und hinzufügen
		if( klang == null )
		{
			// Klang erzeugen
			String pfad = "/" + Spielfeld.class.getPackage().getName().replace( ".", "/" ) + "/";
			klang = Applet.newAudioClip( Spielfeld.class.getResource( pfad + dateiname ) ); 
			
			// Klang in Liste schreiben
			klänge.add( klang );

			// Klangname in liste schreiben
			klangnamen.add( dateiname );
		}
		
		// Wenn wiederholt werden soll, im Loop abspielen
		if( wiederholen )
			klang.loop();
		
		// ...sonst nur einmal abspielen
		else
			klang.play();
	}
	
	
	/**
	 * Spielt den Klang mit dem angegebenen Dateinamen ab. 
	 * 
	 * @param dateiname der Dateiname der abzuspielenden Klangdatei
	 */
	public static void klangAbspielen( String dateiname )
	{
		klangAbspielen( dateiname, false );
	}
	
	
	/**
	 * Stoppt das Abspielen des Klangs mit dem angegebenen Dateinamen.
	 * 
	 * @param dateiname der Dateiname der zu stoppenden Klangdatei
	 */
	public static synchronized void klangStoppen( String dateiname )
	{
		// Klangname in Liste suchen
		for( int i = 0; i < klangnamen.size(); i++ )
		{
			// Wenn gefunden
			if( klangnamen.get( i ).equals( dateiname ) )
			{
				// Zugehörigen Klang stoppen
				klänge.get( i ).stop();
								
				// Schleife verlassen
				break;
			}
		}
	}
	
	
	/**
	 * Lädt ein Bild, wartet bis es geladen ist und gibt es dann als Image-Objekt zurück.
	 * 
	 * @param bildname der Dateiname des zu ladenden Bildes
	 * @return das geladene Bild als Image-Objekt
	 */
	private Image bildLaden( String bildname )
	{
		// Bild für Spielobjekt laden und warten
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		String pfad = "/" + getClass().getPackage().getName().replace( ".", "/" ) + "/";
		Image bild = toolkit.getImage( getClass().getResource( pfad + bildname ) );
		MediaTracker tracker = new MediaTracker( this );
		tracker.addImage( bild, 0 );
		try 
		{
			tracker.waitForID( 0 );
	    } catch (InterruptedException e) 
	    {
	    }
	    
	    return( bild );
	}
}
