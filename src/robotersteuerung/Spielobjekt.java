package robotersteuerung;

import java.awt.Color;
import java.io.Serializable;


/**
 * Objekte dieser Klassen stellen Spielobjekte dar. VERSION: 1.0.4
 * <p>
 * Jedes Spielobjekt hat ein zugehöriges Bild und eine bestimmte Position, 
 * sowie Höhe und Breite. Außerdem kann ein Spielobjekt eine Bewegungsrichtung 
 * und einen Text besitzen. Spielobjekte können auf einem Spielfeld 
 * (also einem Objekt der Klasse "Spielfeld") plaziert werden, indem sie 
 * dem Spielfeld hinzugefügt werden. 
 * <p>
 * Ein einfaches Beispiel für das Hinzufügen und Verwenden eines Spielobjektes 
 * könnte wie folgt aussehen: 
 * <pre>
 * <code>
 * // Spielfeld erzeugen
 * Spielfeld spielfeld = new Spielfeld();
 *     
 * // Spielobjekt erzeugen
 * Spielobjekt spielobjekt = new Spielobjekt();
 *     
 * // Bild für das Spielobjekt festlegen
 * spielobjekt.bildname = "Wolke.png";
 *     
 * // Position des Spielobjektes festlegen
 * spielobjekt.positionX = 50;
 * spielobjekt.positionY = 100;
 *     
 * // Größe des Spielobjektes festlegen
 * spielobjekt.höhe = 30;
 * spielobjekt.breite = 80;
 *	     
 * // Richtung des Spielobjektes festlegen
 * spielobjekt.richtungX = 1;
 *     
 * // Spielobjekt dem Spielfeld hinzufügen
 * spielfeld.hinzufügen( spielobjekt );
 *     
 * // Hauptschleife des Spiels
 * while( true )
 * {
 *     // Das Spielobjekt von links nach rechts bewegen
 *     spielobjekt.positionX = spielobjekt.positionX + spielobjekt.richtungX; 
 *     
 *     // Das Spielfeld neu zeichnen
 *     spielfeld.zeichnen();
 *         
 *     // Etwas warten
 *     spielfeld.warten( 50 );
 * }
 * </code>
 * </pre>
 * Außerdem können neue Spielobjekt-Klassen von der Klasse "Spielobjekt" abgeleitet 
 * werden. Solche eigenen Spielobjekte können wiederum aus Spielobjekten bestehen. 
 * Die Methode "berechnen()" kann dann genutzt werden, um das Verhalten dieser Spielobjekte 
 * zu programmieren. 
 * <p>
 * Die Klasse "Spielobjekt" verwendet das Interface "Serializable", damit Spielobjekte
 * einfach abgespeichert und wieder geladen werden können. So ist es möglich ganze Spielstände
 * zu speichern und wieder zu laden.
 * <p>
 * Beispiel für das Speichern eines Spielobjektes:
 * <pre>
 * <code>
 * try
 * {
 *     // Neue Datei öffnen
 *     ObjectOutputStream datei = new ObjectOutputStream( new FileOutputStream( "test.sav" ) );
 *         
 *     // Spielobjekt speichern
 *     datei.writeObject( spielobjekt );
 *     
 *     // Datei schließen
 *     datei.close();
 * }
 * catch( IOException e )
 * {
 * }
 * </code>
 * </pre>
 * Beispiel für das Laden eines Spielobjektes:
 * <pre>
 * <code>
 * try
 * {
 *     // Datei öffnen
 *     ObjectInputStream datei = new ObjectInputStream( new FileInputStream( "test.sav" ) );
 *         
 *     // Spielobjekt laden
 *     spielobjekt = (Spielobjekt) datei.readObject();
 *     
 *     // Geladenens Spielobjekt dem Spielfeld hinzufügen 
 *     if( spielobjekt != null )
 *         spielfeld.hinzufügen( spielobjekt );
 *         
 *     // Datei schließen
 *     datei.close();
 * }
 * catch( ClassNotFoundException e )
 * {
 * }
 * catch( IOException e )
 * {
 * }
 * </code>
 * </pre>
 * Da auf diese Weise auch alle Unterobjekte und Variablen eines Spielobjektes automatisch 
 * mit gespeichert und geladen werden, ist es am einfachsten alle Spielobjekte als Unterobjekte eines 
 * Hauptspielobjektes zu erstellen und dann nur das Hauptspielobjekt zu speichern und zu laden. 
 * 
 * @see Spielfeld
 * @see Spielfeld#hinzufügen(Spielobjekt)
 * @see Spielobjekt#berechnen()
 * 
 * @author Daan Apeldoorn
 *
 */
public class Spielobjekt implements Serializable
{
	/** Der Dateiname des zum Spielobjekt gehörigen Bildes. */
	public String bildname = "";
	
	/** Die x-Koordinate der linken oberen Ecke des Spielobjektes. */
	public int positionX;

	/** Die y-Koordinate der linken oberen Ecke des Spielobjektes. */
	public int positionY;
	
	/** Die x-Richtung des Spielobjektes. */
	public int richtungX;

	/** Die y-Richtung des Spielobjektes. */
	public int richtungY;

	/** Die Höhe des Spielobjektes. */
	public int höhe;

	/** Die Breite des Spielobjektes. */
	public int breite;
	
	/** Der Text, der zusätzlich zum Spielobjekt angezeigt werden soll. */
	public String text = "";

	/** Die Farbe des Textes, der zusätzlich zum Spielobjekt angezeigt werden soll. */
	public Color textfarbe;

	/** Gibt an, ob ein Spielobjekt sichtbar ist. */
	public boolean unsichtbar;

	
    /** 
     * Gibt zurück ob das Spielobjekt das andere Spielobjekt berührt.
     * 
     * @param spielobjekt das Spielobjekt, mit welchem auf Berührung geprüft wird
     * @return ob das Spielobjekt berührt wird oder nicht
     */
    public boolean berührt( Spielobjekt spielobjekt )
    {
        // Für jede Ecke des Spielobjekts prüfen, ob sie innerhalb des anderen Spielobjekts liegt und umgekehrt oder ob das eine über dem anderen Spielobjekt liegt (ohne die Ecken)!
        if(   (positionX >= spielobjekt.positionX && positionX <= spielobjekt.positionX + spielobjekt.breite - 1 && positionY >= spielobjekt.positionY && positionY <= spielobjekt.positionY + spielobjekt.höhe - 1) 
           || (positionX + breite - 1 >= spielobjekt.positionX && positionX + breite - 1 <= spielobjekt.positionX + spielobjekt.breite - 1 && positionY >= spielobjekt.positionY && positionY <= spielobjekt.positionY + spielobjekt.höhe - 1) 
           || (positionX + breite - 1 >= spielobjekt.positionX && positionX + breite - 1 <= spielobjekt.positionX + spielobjekt.breite - 1 && positionY + höhe - 1 >= spielobjekt.positionY && positionY + höhe - 1 <= spielobjekt.positionY + spielobjekt.höhe - 1) 
           || (positionX >= spielobjekt.positionX && positionX <= spielobjekt.positionX + spielobjekt.breite - 1 && positionY + höhe - 1 >= spielobjekt.positionY && positionY + höhe - 1 <= spielobjekt.positionY + spielobjekt.höhe - 1) 
           || (positionX >= spielobjekt.positionX && positionX <= spielobjekt.positionX + spielobjekt.breite - 1 && positionX + breite - 1 <= spielobjekt.positionX + spielobjekt.breite - 1 && positionY <= spielobjekt.positionY && positionY + höhe - 1 >= spielobjekt.positionY + spielobjekt.höhe - 1)
           || (spielobjekt.positionX >= positionX && spielobjekt.positionX <= positionX + breite - 1 && spielobjekt.positionY >= positionY && spielobjekt.positionY <= positionY + höhe - 1) 
           || (spielobjekt.positionX + spielobjekt.breite - 1 >= positionX && spielobjekt.positionX + spielobjekt.breite - 1 <= positionX + breite - 1 && spielobjekt.positionY >= positionY && spielobjekt.positionY <= positionY + höhe - 1) 
           || (spielobjekt.positionX + spielobjekt.breite - 1 >= positionX && spielobjekt.positionX + spielobjekt.breite - 1 <= positionX + breite - 1 && spielobjekt.positionY + spielobjekt.höhe - 1 >= positionY && spielobjekt.positionY + spielobjekt.höhe - 1 <= positionY + höhe - 1) 
           || (spielobjekt.positionX >= positionX && spielobjekt.positionX <= positionX + breite - 1 && spielobjekt.positionY + spielobjekt.höhe - 1 >= positionY && spielobjekt.positionY + spielobjekt.höhe - 1 <= positionY + höhe - 1) 
           || (spielobjekt.positionX >= positionX && spielobjekt.positionX <= positionX + breite - 1 && spielobjekt.positionX + spielobjekt.breite - 1 <= positionX + breite - 1 && spielobjekt.positionY <= positionY && spielobjekt.positionY + spielobjekt.höhe - 1 >= positionY + höhe - 1) )
            return( true );
        else
            return( false );
    }
    
    
	/**
	 * Gibt eine Zufallszahl im angegebenen Bereich zurück (jeweils einschließlich der angegebenen Grenzen).
	 * 
	 * @param von Untergrenze des Bereichs
	 * @param bis Obergrenze des Bereichs
	 * @return eine Zufallszahl im angegebenen Bereich (jeweils einschließlich der angegebenen Grenzen)
	 * 
	 * @see Spielfeld#zufallszahl( int, int )
	 */
	public int zufallszahl( int von, int bis )
	{
		return( Spielfeld.zufallszahl( von, bis ) );
	}

	
	/**
	 * Spielt den Klang mit dem angegebenen Dateinamen ab. 
	 * <p>
	 * Es kann festgelegt werden, ob der Klang einmalig oder fortlaufend in einer 
	 * Schleife abgespielt werden soll.
	 * 
	 * @param dateiname der Dateiname der abzuspielenden Klangdatei
	 * @param wiederholen legt fest, ob der Klang einmalig oder fortlaufend in einer Schleife abgespielt werden soll
	 */
	public synchronized void klangAbspielen( String dateiname, boolean wiederholen )
	{
		Spielfeld.klangAbspielen( dateiname, wiederholen );
	}

	
	/**
	 * Spielt den Klang mit dem angegebenen Dateinamen ab. 
	 * 
	 * @param dateiname der Dateiname der abzuspielenden Klangdatei
	 */
	public void klangAbspielen( String dateiname )
	{
		klangAbspielen( dateiname, false );
	}
	
	
	/**
	 * Stoppt das Abspielen des Klangs mit dem angegebenen Dateinamen.
	 * 
	 * @param dateiname der Dateiname der zu stoppenden Klangdatei
	 */
	public synchronized void klangStoppen( String dateiname )
	{
		Spielfeld.klangStoppen( dateiname );
	}

	
	/**
	 * Neue Spielobjekt-Klassen, die von der Klasse "Spielobjekt" abgeleitet werden können
	 * diese Methode füllen, um Berechnungen wie Bewegung oder anderes Verhalten
	 * umzusetzen.
	 */
	public void berechnen()
	{
	}
}
