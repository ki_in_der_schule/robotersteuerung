package robotersteuerung;

import java.awt.Color;


public class Main {

	public static void main( String[] args ) 
	{
		// Spielfeld erzeugen
		Spielfeld spielfeld = new Spielfeld();
		
		// Hintergrund erzeugen
		Spielobjekt hintergrund = new Spielobjekt();
		hintergrund.bildname = "Wiese.png";
		hintergrund.breite = Spielfeld.breite;
		hintergrund.höhe = Spielfeld.höhe;
		hintergrund.positionX = 0;
		hintergrund.positionY = 0;
		spielfeld.hinzufügen( hintergrund );

		Spielobjekt hintergrund2 = new Spielobjekt();
		hintergrund2.bildname = "Wiese.png";
		hintergrund2.breite = Spielfeld.breite;
		hintergrund2.höhe = Spielfeld.höhe;
		hintergrund2.positionX = 0;
		hintergrund2.positionY = hintergrund.positionY - hintergrund2.höhe + 1;
		spielfeld.hinzufügen( hintergrund2 );
		
		// Strecke
		double[] strecke = new double[]{  0, 10, 20, 30, 50, 70, 90, 120, 150, 170, 190, 200, 210, 200, 
				                         190, 180, 160, 140, 120, 90, 70, 50, 40, 30, 10, 0, 
				                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		
		// Straße erzeugen
		final int ANZ_STR_ELEMENTE = 20;
		Spielobjekt[] straße = new Spielobjekt[ strecke.length ];
		for( int i = 0; i < straße.length; i++ )
		{
			straße[ i ] = new Spielobjekt();
			straße[ i ].bildname = "Straße.png";			
			straße[ i ].textfarbe = Color.DARK_GRAY;			
			straße[ i ].breite = 200;
			straße[ i ].höhe = Spielfeld.höhe / ANZ_STR_ELEMENTE;
			straße[ i ].positionX = Spielfeld.breite / 2 - straße[ i ].breite / 2 + (int) strecke[ i ];
			straße[ i ].positionY = Spielfeld.höhe - (i + 1) * (Spielfeld.höhe / ANZ_STR_ELEMENTE);
			spielfeld.hinzufügen( straße[ i ] );
			if( i > 0 )
			{
				String kurvenBeschreibung = "";
				if( straße[ i - 1 ].positionX - straße[ i ].positionX <= -20 )
				{
					kurvenBeschreibung = "stark rechts";
				}
				else if( straße[ i - 1 ].positionX - straße[ i ].positionX <= -10 )
				{
					kurvenBeschreibung = "rechts";
				}
				else if( straße[ i - 1 ].positionX - straße[ i ].positionX == 0 )
				{
					kurvenBeschreibung = "gerade";
				}
				else if( straße[ i - 1 ].positionX - straße[ i ].positionX <= 10 )
				{
					kurvenBeschreibung = "links";
				}
				else
				{
					kurvenBeschreibung = "stark links";
				}
				
				straße[ i - 1 ].text = "                                                                               " + kurvenBeschreibung + " ("+ (straße[ i - 1 ].positionX - straße[ i ].positionX) + ")"; 
			}
		}
		
		// Roboter erzeugen
		Spielobjekt roboter = new Spielobjekt();
		roboter.bildname = "Roboter.png";
		roboter.textfarbe = Color.WHITE;
		roboter.breite = 50;
		roboter.höhe = 50;
		roboter.positionX = Spielfeld.breite / 2 - roboter.breite / 2;
		roboter.positionY = Spielfeld.höhe - roboter.höhe - 5;
		spielfeld.hinzufügen( roboter );
				
		// Robotersteuerung erzeugen
		RoboterSteuerung roboterSteuerung = new RoboterSteuerung();		
		
		// HAUPTSCHLEIFE
		Spielobjekt kurvenAbschnitt = null;
		while( true )
		{
			// Roboter bewegen
			int roboterGeschwindigkeit = 8;
			if( kurvenAbschnitt == null || roboter.positionX < kurvenAbschnitt.positionX || roboter.positionX + roboter.breite > kurvenAbschnitt.positionX + kurvenAbschnitt.breite )
			{
				roboterGeschwindigkeit = 1;
			}
			for( int i = 0; i < straße.length; i++ )
			{
				straße[ i ].positionY += roboterGeschwindigkeit;
			}
			hintergrund.positionY += roboterGeschwindigkeit;
			hintergrund2.positionY += roboterGeschwindigkeit;
			if( hintergrund.positionY >= Spielfeld.höhe )
			{
				hintergrund.positionY = hintergrund2.positionY - hintergrund.höhe + 1;
			}
			else if( hintergrund2.positionY >= Spielfeld.höhe )
			{
				hintergrund2.positionY = hintergrund.positionY - hintergrund2.höhe + 1;
			}
			roboter.positionX += roboter.richtungX;
			
			// Momentanen Kurvenabschnitt ermitteln
			kurvenAbschnitt = null;
			for( int i = straße.length - 1; i > 0; i-- )
			{
				if( straße[ i ].berührt( roboter ) )
				{
					kurvenAbschnitt = straße[ i ];
					
					int roboterXPos = (roboter.positionX + (roboter.breite / 2)) - (straße[ i ].positionX + (straße[ i ].breite / 2)); 
					if( roboterXPos < -(straße[ i ].breite / 5) )
						roboter.text = "weit links";
					else if( roboterXPos < 0 )
						roboter.text = "links";
					else if( roboterXPos == 0 )
						roboter.text = "mittig";
					else if( roboterXPos < (straße[ i ].breite / 5) )
						roboter.text = "rechts";
					else
						roboter.text = "weit rechts";
					
					break;
				}
			}
			
			if( kurvenAbschnitt != null )
			{
				// Roboterposition auf der Straße ermitteln
				double roboterXPos = 0;
				for( int i = straße.length - 1; i > 0; i-- )
				{
					if( straße[ i ].berührt( roboter ) )
					{
						roboterXPos = (((double) 20) * ((roboter.positionX + (roboter.breite / 2)) - (straße[ i ].positionX + (straße[ i ].breite / 2)))) / (straße[ i ].breite / 2.0); 
						break;
					}
				}
								
				// Robotersteuerung berechnen und begrenzen
				roboter.richtungX = roboterSteuerung.berechnen( roboterXPos );
				if( roboter.richtungX < -20 )
				{
					roboter.richtungX = -20;
				}
				else if( roboter.richtungX > 20 )
				{
					roboter.richtungX = 20;
				}
			}
			
			// Spielfeld aktualisierten
			spielfeld.zeichnen();

			// Etwas warten
			spielfeld.warten( 50 );
		}
	}

}
