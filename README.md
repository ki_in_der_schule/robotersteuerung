# RoboterSteuerung

Ein Java-Simulationswerkzeug (Eclipse-Projekt) zur Steuerung eines Roboters entlang einer Straße mit unterschiedlichen Ansätzen (zum Beispiel "harte" Logik versus Fuzzy-Logik).